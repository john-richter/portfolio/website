
# Add `-resize XX%` to resize an image

BACKUP_DIR=~/Desktop/UnoptimizedGifs
mkdir -p ${BACKUP_DIR}

for F in $(find ./content -name "*.gif"); do
    FILENAME=${F%.*}
    FILENAME_EXT=${F##*/}
    EXPORTED_FILE=$(realpath ${BACKUP_DIR}/${FILENAME_EXT})
    mv ${F} ${EXPORTED_FILE}
    magick convert ${EXPORTED_FILE} -monitor -coalesce  -border 0 -border 0 -layers Optimize ${FILENAME}.gif
done

for F in $(find ./static -name "*.gif"); do
    FILENAME=${F%.*}
    FILENAME_EXT=${F##*/}
    EXPORTED_FILE=$(realpath ${BACKUP_DIR}/${FILENAME_EXT})
    mv ${F} ${EXPORTED_FILE}
    magick convert ${EXPORTED_FILE} -monitor -coalesce  -border 0 -layers Optimize ${FILENAME}.gif
done

for F in $(find ./src -name "*.gif"); do
    FILENAME=${F%.*}
    FILENAME_EXT=${F##*/}
    EXPORTED_FILE=$(realpath ${BACKUP_DIR}/${FILENAME_EXT})
    mv ${F} ${EXPORTED_FILE}
    magick convert ${EXPORTED_FILE} -monitor -coalesce  -border 0 -layers Optimize ${FILENAME}.gif
done
