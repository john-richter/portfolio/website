const fs = require('fs');
const path = require('path');

module.exports = async ({ markdownAST, cache, getCache }, pluginOptions) => {
    // Load all the themes into the gatsby-remark-vscode plugin's cache
    const themesPath = path.resolve(__dirname, './themes');
    const themes = {};
    for (const themeFile of fs.readdirSync(themesPath, 'utf8')) {
        const themePath = path.resolve(themesPath, themeFile);
        // const themeRaw = fs.readFileSync(themePath);
        // const themeJson = JSON.parse(themeRaw);
        const name = path.basename(themeFile, '.json');
        const id = name.toLowerCase();
        const label = name.replace(/-/g, ' ');
        themes[id] = { id, label, path: themePath };
    }
    const gatsbyRemarkVSCodeCache = await getCache('gatsby-remark-vscode');
    await gatsbyRemarkVSCodeCache.set('themes', themes);
    return markdownAST;
}
