const fs = require('fs');
const path = require('path');
const visit = require('unist-util-visit');

module.exports = ({ markdownAST, cache, getCache }, pluginOptions) => {
    // Wraps vscode-highlights in a container just like gatsby does with prismjs <pre> elements
    function visitor(node) {
        // `null` is a valid value
        if (node.lang !== undefined && node.value.startsWith('<pre') &&
            // node.value.includes('vscode-highlight')
            node.value.includes('grvsc-container')) {
            node.value = `<div class="grvsc-container-wrapper">${node.value}</div>`
        }
    }
    visit(markdownAST, 'html', visitor);
    return markdownAST;
}
