const React = require('react');
const { IMAGE_CLASS } = require(`./constants`);

exports.onRenderBody = ({ setHeadComponents }) => {
    // This code came from gatsby-remark-images. Meant to keep image loading consistent across my plugin and theirs
    const style = `
    .${IMAGE_CLASS} {
        width: 100%;
        height: 100%;
        margin: 0;
        vertical-align: middle;
        position: absolute;
        top: 0;
        left: 0;
        color: transparent;
    }`
        .replace(/\s*\n\s*/g, ``)
        .replace(/: /g, `:`)
        .replace(/ \{/g, `{`);

    setHeadComponents([
        <style type="text/css" key="gatsby-remark-rewrap-images-styles">{style}</style>,
    ]);
};