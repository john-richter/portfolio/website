import { CommonShareButtonProps } from 'react-share';

declare module 'react-share' {
    export interface CommonShareButtonProps {
        className?: string;
    }
}

