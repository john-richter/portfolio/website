import { Theme as MUITheme } from '@material-ui/core';
import { makeStyles as muiCoreMakeStyles } from '@material-ui/core';
import { Styles, ClassNameMap, ClassKeyOfStyles, WithStylesOptions } from '@material-ui/styles/withStyles';
import { Nullable } from './utils';

/**
 * Styled with Hooks
 */

/**
 * Return type of `makeStyles()`
 *
 * Due to the way that MUI changed their type definitions, this isn't posible to type anymore :(
 */
// export type MakeStyles<ClassKey extends string = string, Props extends {} = {}> =
//     Props extends {} ? (props?: any) => ClassNameMap<ClassKey> : (props: TProps) => ClassNameMap<ClassKey>;


// StylesHook<
//     Styles<MUITheme, Props extends never ? {} : Props, ClassKey>
// >;

/**
 * Return type of the call to the hook in render, i.e. `makeStyles()()`
 */
export type UseStyles<ClassKey extends string = '', Props extends {} = {}> = ClassNameMap<
    ClassKeyOfStyles<Styles<MUITheme, Props extends never ? {} : Props, ClassKey>>
>;

// /**
//  * An alias function to make working with the order of template arguments easier for makeStyles
//  */
// export function makeStyles<ClassKey extends string = string, Props extends object = {}>(
//     styles: Styles<MUITheme, Props, ClassKey>,
//     options?: Omit<WithStylesOptions<MUITheme>, 'withTheme'>,
// ): (props: Props) => ClassNameMap<ClassKey>;
// export function makeStyles<ClassKey extends string = string, Props extends object = {}>(
//     styles: Styles<MUITheme, {}, ClassKey>,
//     options?: Omit<WithStylesOptions<MUITheme>, 'withTheme'>,
// ): (props?: any) => ClassNameMap<ClassKey> {
//     return muiCoreMakeStyles<MUITheme, ClassKey>(styles, options);
// }
