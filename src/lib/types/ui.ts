export type TWindowDimensions = { width: number, height: number };
