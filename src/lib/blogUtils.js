const routes = require('../config/routes');

module.exports = {
    // Converts title-case, space separated tags into hyphenated, lowercase slugs with trailing slashes
    tagPath(t) {
        const path = `${routes.BLOG_TOPIC_PATH_PREFIX}${t.toLowerCase().replace(/\s/g, '-')}`;
        return `${path}${path.endsWith('/') ? '' : '/'}`;
    }
};