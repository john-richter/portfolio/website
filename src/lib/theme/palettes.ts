
export type TextUIColor = {
    light: string;
    lighter: string;
    main: string;
    dark: string;
    darker: string;
}

export type UIColor = {
    color: string;
    contrastText: TextUIColor;
    contrastIconText: TextUIColor;
}

export type PaletteColor = {
    50: UIColor;
    100: UIColor;
    200: UIColor;
    300: UIColor;
    400: UIColor;
    500: UIColor;
    600: UIColor;
    700: UIColor;
    800: UIColor;
    900: UIColor;
};

export type BaseUIPalette = {
    black: string;
    white: string;
};

export type HomePalette = {
    greeting: string;
};

export type BlogPalette = {
    headingLink: UIColor;
};

export type ProjectsPalette = {
    previewCard: {
        stack: UIColor;
    }
};

export type HeaderUIPalette = {
    default: UIColor;
    home: {
        main: string;
        hover: string;
    }
    nav: {
        hover: string;
    }
};

export type FooterUIPalette = {
    default: UIColor;
    divider: string;
    links: {
        hover: string;
    };
}

// VSCode themeing takes care of the main block palette
export type CodeBlockUIPalette = {
    insetShadowColor: string;
    lineHighlight: {
        background: UIColor;
        accent: UIColor;
    };
}

export type CodeUIPalette = {
    inline: UIColor;
    block: CodeBlockUIPalette;
}

export type QuoteUIPalette = {
    text: UIColor;
    border: UIColor;
}

export type ToolipUIPalette = {
    border: UIColor;
    background: UIColor;
}
export type TextUIPalette = {
    color: TextUIColor;
    icon: TextUIColor;
    selection: UIColor;
}

export type CardUIPalette = {
    labelArea: UIColor;
};

export type TabsUIPalette = {
    default: string;
    disabled: string;
    selected: string;
    hover: string;
    indicator: string;
}

export type SocialMediaBrandUIPalette = {
    linkedIn: string;
    twitter: string;
    facebook: string;
    instagram: string;
    youtube: string;
    spotify: string;
    github: string;
    gitlab: string;
    reddit: string;
    keybase: string;
    tumblr: string;
    fitbit: string;
    jawbone: string;
    runkeeper: string;
}

export type DividerUIPalette = {
    main: string;
    light: string;
};

export type LinksUIPalette = {
    default: UIColor;
    hover: string;
}

export type AvatarUIPalette = {
    icon: {
        background: string;
        color: string;
    };
    insetBoxShadow: string;
};

export interface UIPalette extends BaseUIPalette {
    pages: {
        home: HomePalette
        blog: BlogPalette;
        projects: ProjectsPalette;
    }
    brands: SocialMediaBrandUIPalette;
    code: CodeUIPalette;
    background: { default: UIColor, paper: UIColor };
    text: TextUIPalette;
    links: LinksUIPalette;
    divider: DividerUIPalette;
    quote: QuoteUIPalette;
    tooltip: ToolipUIPalette;
    header: HeaderUIPalette;
    footer: FooterUIPalette;
    avatar: AvatarUIPalette;
    card: CardUIPalette;
    tabs: TabsUIPalette;
}

export interface LightUIPalette extends UIPalette {
    type: 'light';
    neutral: PaletteColor; // Also primary
    lightBlue: PaletteColor;
    cyan: PaletteColor;
    pink: PaletteColor;
    red: PaletteColor;
    yellow: PaletteColor;
    teal: PaletteColor;
}

export interface DarkUIPalette extends UIPalette {
    type: 'dark';
}

export type UIPalettes = LightUIPalette | DarkUIPalette;
