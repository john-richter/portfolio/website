import { Shape } from '@material-ui/core/styles/shape';

export type UIShape = {};

export const SHAPE: Shape = {
    borderRadius: 4,
};
