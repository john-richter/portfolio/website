import createBreakpoints, { Breakpoints } from '@material-ui/core/styles/createBreakpoints';

export const BREAKPOINTS: Breakpoints = createBreakpoints({});
