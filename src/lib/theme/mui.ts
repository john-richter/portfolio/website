import { Palette, PaletteOptions } from '@material-ui/core/styles/createPalette';
import { Typography, TypographyOptions, TypographyUtils, TypographyStyle, FontStyle, Variant as ThemeStyle } from '@material-ui/core/styles/createTypography';
import { Theme, ThemeOptions } from '@material-ui/core/styles/createMuiTheme';
import { UIPalettes } from './palettes';
import { TypeSizes, TypeSize, FontTypes, IconSizes, FontFeatureSettings } from './typography';
import { CSSProperties } from '@material-ui/styles';
import { Shape } from '@material-ui/core/styles/shape';
import { UIShape } from './shape';
import { Baseline } from './baseline';

declare module '@material-ui/core/styles/createPalette' {
    export interface Palette {
        custom: UIPalettes;
    }

    export interface PaletteOptions {
        custom: UIPalettes;
    }
}

declare module '@material-ui/core/styles/createTypography' {
    export type CustomThemeStyles = 'body1Mono' | 'body2Mono' | 'captionMono' | 'buttonMono' | 'overlineMono';
    export type CustomThemeStyle = ThemeStyle | CustomThemeStyles;

    export interface Typography extends Record<CustomThemeStyle, TypographyStyle>, FontStyle, TypographyUtils {
        fontFeatureSettings: FontFeatureSettings;
        fontFamilyHeadings: Required<CSSProperties['fontFamily']>;
        fontFamilyMono: Required<CSSProperties['fontFamily']>;
        blockquote: TypographyStyle;
        sizing(size: TypeSizes, font?: FontTypes): TypeSize;
        iconSizing(size: IconSizes): TypeSize;
    }

    export interface TypographyOptions extends
        Partial<Record<CustomThemeStyle, TypographyStyleOptions> & FontStyleOptions>, Partial<TypographyUtils> {
        fontFeatureSettings: FontFeatureSettings;
        fontFamilyHeadings: Required<CSSProperties['fontFamily']>;
        fontFamilyMono: Required<CSSProperties['fontFamily']>;
        blockquote: TypographyStyle;
        sizing(size: TypeSizes, font?: FontTypes): TypeSize;
        iconSizing(size: IconSizes): TypeSize;
    }
}

declare module '@material-ui/core/styles/shape' {
    export interface Shape extends UIShape { }
}

declare module '@material-ui/core/styles/createMuiTheme' {
    export interface Theme {
        baseline: Baseline;
    }

    export interface ThemeOptions {
        baseline: Baseline;
    }
}