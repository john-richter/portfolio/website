import MUIShadows, { Shadows } from '@material-ui/core/styles/shadows';

export const SHADOWS: Shadows = [
    MUIShadows[0],

    MUIShadows[3],
    MUIShadows[3],
    MUIShadows[3],

    MUIShadows[6],
    MUIShadows[6],
    MUIShadows[6],

    MUIShadows[9],
    MUIShadows[9],
    MUIShadows[9],

    MUIShadows[12],
    MUIShadows[12],
    MUIShadows[12],

    MUIShadows[15],
    MUIShadows[15],
    MUIShadows[15],

    MUIShadows[18],
    MUIShadows[18],
    MUIShadows[18],

    MUIShadows[21],
    MUIShadows[21],
    MUIShadows[21],

    MUIShadows[24],
    MUIShadows[24],
    MUIShadows[24]
];
