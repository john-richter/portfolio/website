import { globalHistory } from '@reach/router';

export function getPageSlug(trailingSlash: boolean = true): string {
    let slug: string = globalHistory.location.pathname;
    // Force trailing slash
    if (trailingSlash && !slug.endsWith('/')) {
        slug = `${slug}/`;
    }
    return slug;
}
