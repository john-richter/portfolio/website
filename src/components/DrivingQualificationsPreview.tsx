import * as React from 'react';
import { useStaticQuery, graphql } from 'gatsby';
import { TReactFCP, TReactFCR } from '../lib/types/utils';
import { Grid, Theme, makeStyles } from '@material-ui/core';
import GatsbyImage from 'gatsby-image/withIEPolyfill';
import { GQLDrivingQualificationsPreviewQuery } from '../lib/types/graphql/__generated__/gatsby.gql';
import { UseStyles } from '../lib/types/mui';

type ClassKey = 'imageContainer' | 'grid' | 'item';
const useStyles = makeStyles<Theme, ClassKey>((theme: Theme) => ({
    grid: {
        maxHeight: '100px', height: 'auto', width: 'auto', overflow: 'hidden'
    },
    item: {
        height: '100%', width: '100%'
    },
    imageContainer: {
        height: '100%', width: '100%'
    },
    image: {
        // height: '100%',
        // '& > div:first-child': { paddingBottom: 0 }
    },
}));

export type TDrivingQualificationsPreviewP = {};

export function DrivingQualificationsPreview(props: TReactFCP<TDrivingQualificationsPreviewP>): TReactFCR {
    const styles: UseStyles<ClassKey> = useStyles();
    // const {
    //     msfCard, msfL1, sccaInstructorCert
    // }: GQLDrivingQualificationsPreviewQuery = useStaticQuery(graphql`
    //     query DrivingQualificationsPreview {
    //         msfCard: file(relativePath: {eq: "driving/qualifications/MSF-Certification-Card.png"}) {
    //             childImageSharp {
    //                 fluid(maxHeight: 100) {
    //                     ...GatsbyImageSharpFluid_withWebp
    //                 }
    //             }
    //         }
    //         msfL1: file(relativePath: {eq: "driving/qualifications/MSF-L1-Certificate.jpeg"}) {
    //             childImageSharp {
    //                 fluid(maxHeight: 100) {
    //                     ...GatsbyImageSharpFluid_withWebp
    //                 }
    //             }
    //         }
    //         sccaInstructorCert: file(relativePath: {eq: "driving/qualifications/SCCA-Instructor-Certificate.jpeg"}) {
    //             childImageSharp {
    //                 fluid(maxHeight: 100, background: "rgba(0, 0, 0, 0)") {
    //                     ...GatsbyImageSharpFluid_withWebp
    //                 }
    //             }
    //         }
    //     }
    // `);
    return (
        <Grid className={styles.grid} container wrap='nowrap' spacing={4} alignItems='flex-start'>
            <Grid className={styles.item} item xs>
                <div className={styles.imageContainer}>
                    {/* <GatsbyImage
                        fluid={sccaInstructorCert!.childImageSharp!.fluid!}
                        objectFit='cover'
                        objectPosition='center center'
                        style={{ maxWidth: '100%', maxHeight: '100%', width: 'auto', height: 'auto' }}
                        // imgStyle={{ maxWidth: '100%', maxHeight: '100%', width: 'auto', height: 'auto' }}
                        imgStyle={{ width: '100%', height: '100%' }}
                    /> */}
                </div>
            </Grid>
            <Grid className={styles.item} item xs>
                <div className={styles.imageContainer}>
                    {/* <GatsbyImage
                        fluid={msfL1!.childImageSharp!.fluid!}
                        objectFit='cover'
                        objectPosition='center center'
                        style={{ maxWidth: '100%', maxHeight: '100%', width: 'auto', height: 'auto' }}
                        imgStyle={{ maxWidth: '100%', maxHeight: '100%', width: 'auto', height: 'auto' }}
                    // imgStyle={{ width: '100%', height: '100%' }}
                    /> */}
                </div>
            </Grid>
            <Grid className={styles.item} item xs>
                <div className={styles.imageContainer}>
                    {/* <GatsbyImage
                        fluid={msfCard!.childImageSharp!.fluid!}
                        objectFit='cover'
                        objectPosition='center center'
                        style={{ maxWidth: '100%', maxHeight: '100%', width: 'auto', height: 'auto' }}
                        imgStyle={{ maxWidth: '100%', maxHeight: '100%', width: 'auto', height: 'auto' }}
                    // imgStyle={{ width: '100%', height: '100%' }}
                    /> */}
                </div>
            </Grid>
        </Grid>
    );
}