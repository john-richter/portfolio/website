import * as React from 'react';
import { TReactFCP, TReactFCR } from '../../../../lib/types/utils';
import { observer } from 'mobx-react';
import { Theme, List, makeStyles } from '@material-ui/core';
import { UseStyles } from '../../../../lib/types/mui';
import { TNavLayoutState, ILayoutStore, useLayoutStore } from '../../../../lib/stores/layout';
import { NAV_LINKS } from '../../../../config/nav';
import { NavLinkConfig, NavLink, TNavLinkP } from './NavLink';

type StyleProps = TNavLayoutState;

type ClassKeys = 'root';
const useStyles = makeStyles<Theme, StyleProps, ClassKeys>((theme: Theme) => ({ root: {} }));

export type TNavFooterP = {
    NavLinkProps: Omit<TNavLinkP, 'variant'>;
};

export const NavFooter: React.FC<TNavFooterP> = observer((props: TReactFCP<TNavFooterP>): TReactFCR => {
    const store: ILayoutStore = useLayoutStore();
    const styles: UseStyles<ClassKeys, StyleProps> = useStyles(store.currentNavLayout);
    const links: React.ReactNode[] = NAV_LINKS.map((c: NavLinkConfig) => (
        <NavLink key={c.path} variant='list' config={c} disableIcon />
    ));
    return <nav className={styles.root}><List>{links}</List></nav>;
});
