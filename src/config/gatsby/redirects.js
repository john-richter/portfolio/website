/**
 * These are global redirects which are loaded in gatsby-node.js
 */

module.exports = [
    {
        fromPath: '/v1',
        toPath: 'https://deploy-v1--jrichter-io.netlify.com',
        isPermanent: false,
        redirectInBrowser: true,
    },
    {
        fromPath: '/v1/',
        toPath: 'https://deploy-v1--jrichter-io.netlify.com',
        isPermanent: false,
        redirectInBrowser: true,
    }
];