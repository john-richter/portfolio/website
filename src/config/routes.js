
module.exports = {
    BLOG_PATH_PREFIX: '/blog/',
    BLOG_TOPIC_PATH_PREFIX: '/blog/topic/',
    BLOG_RSS_FEED_PATH: '/blog/rss.xml'
};
