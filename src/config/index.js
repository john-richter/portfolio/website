// This module must be imported after `dotenv` has been required

const {
    NODE_ENV,
    GATSBY_SITE_FQDN,
    // This URL represents the main address to your site. It can be either a Netlify subdomain or your own custom
    // domain if you set one; for example, https://petsof.netlify.com or https://www.petsofnetlify.com.
    URL: SITE_URL_CANONICAL = GATSBY_SITE_FQDN || 'https://jrichter.io', // Must not include trailing slash
    // This URL represents the primary URL for an individual deploy, or a group of them, like branch deploys and Deploy
    // Previews; for example, https://feature-branch--petsof.netlify.com
    DEPLOY_PRIME_URL: SITE_URL_CONTEXT = SITE_URL_CANONICAL,
    // This URL represents the unique URL for an individual deploy. It starts with a unique ID that identifies the
    // deploy; for example, https://5b243e66dd6a547b4fee73ae--petsof.netlify.com.
    DEPLOY_URL: SITE_URL_UNIQUE = SITE_URL_CONTEXT,
    // Name of the build's deploy context. It can be `production`, `deploy-preview` or `branch-deploy`.
    CONTEXT: DEPLOYMENT_ENV = NODE_ENV
} = process.env;

// The goal here is to use the most specific URL possible for a deployment. This means that we prefer them in the
// following order
//   1. Hashed URL - https://5b243e66dd6a547b4fee73ae--jrichter-io.netlify.com
//   2. Context URL - https://deploy-preview-21--jrichter-io.netlify.com
//   3. Production URL - https://jrichter.io
// This is so that URLs are generated correctly for every deployment, whether its a one-off, branch preview, deploy
// preview, production, etc. Canonical is defaulted to throughout the app for places where it makes sense - social
// sharing links, SEO, RSS feed, etc.
const isProduction = DEPLOYMENT_ENV === 'production';
const siteUrl = isProduction ? SITE_URL_CANONICAL : SITE_URL_UNIQUE;

module.exports = {
    analytics: {
        facebook: {
            appId: '561903224379541'
        },
        google: {
            tagManager: 'GTM-WC83ZXN'
        }
    },
    env: DEPLOYMENT_ENV,
    site: {
        // Manifest. Valid values: `query`(default), `name`, or `none`
        cacheBustingMode: 'query',
        // Enables multiple deployments of the site without messing up SEO if the non-canonical deployments are crawled
        // or linked to in some way
        canonicalUrl: SITE_URL_CANONICAL,
        // Manifest. Valid values are `use-credentials` or `anonymous`.
        // https://www.gatsbyjs.org/packages/gatsby-plugin-manifest/#enable-cors-using-crossorigin-attribute
        crossOrigin: 'anonymous',
        // Used for SEO
        logo: {
            src: '/images/appLogo.png',  // Relative to /static
            width: 512,
            height: 512
        },
        // Manifest. Used for generation of favicons.
        icon: {
            // Must be relative to root directory of project. Needs to be square, a JPEG, PNG, WebP, TIFF, GIF or SVG,
            // and at least 512 x 512.
            path: './static/images/appLogo.png',
            // For a list of available options see
            // https://developer.mozilla.org/en-US/docs/Web/Manifest/icons
            // https://w3c.github.io/manifest/#purpose-member
            options: {}
        },
        // Manifest and siteMetadata. This should help with i18n if it makes sense to implement in the future
        i18n: {
            native: {
                // RSS Feed Generation
                blog: {
                    injectRssFeedInPagesMatchingRegex: '^/blog/',
                    rssFeed: '/blog/rss.xml',
                    title: `John Richter's Blog RSS Feed`,
                    slug: `/blog/`,
                },
                // Site metadata, SEO, Manifest. Twitter seems to like < 125 characters for large cards
                description: `
                    Velocity enthusiast, software engineer, and entrepreneur building a microstartup in amateur
                    motorsports. Join me on track!
                `.trim().replace(/[\s\n]+/g, ' '),
                entrypoint: '/',                        // Manifest
                imageSeo: {                             // Page SEO
                    src: '/images/social-image.jpg',    // Relative to /static
                    width: 1080,
                    height: 565
                },
                language: 'en',                 // Manifest
                name: 'John Richter',           // Used for random stuff like copywrite text, etc
                title: 'John Richter',          // Page title, etc
                titleShort: 'johnrichter',      // Manifest
                titleSeo: 'John Richter',       // Page SEO title
            },
            // Each entry should hae identical fields as `native`
            foreign: []
        },
        // TODO: Add more for SEO?
        keywords: ['jrichte43', 'johnrichter', 'johnathanrichter', 'jrichter'],
        // Manifest
        theme: {
            backgroundColor: '#F0F4F8',
            themeColor: '#102A43',
            // TODO: Switch to false and add the <meta> tag in helmet programatically when the theme is changed
            // See https://www.gatsbyjs.org/packages/gatsby-plugin-manifest/#remove-theme-color-meta-tag
            colorInHead: true,
        },
        url: siteUrl,
    },
    social: {
        email: 'connect@jrichter.io',           // Hit me up!
        facebook: 'richter.johnathan',
        fitbit: '4B2L7J',                       // Don't actively use
        github: 'johnrichter',
        gitlab: 'johnrichter',
        instagram: 'jrichte43',                 // Don't actively use, but thinking about it
        keybase: 'johnr',
        linkedin: 'johnathanrichter',
        reddit: 'jrichte43',                    // Don't actively use
        spotify: 'j-richter',
        tumblr: 'john-richter',                 // Don't actively use
        twitter: 'jrichte43',                   // Hit me up!
        youtube: 'UCvT4dLIWDFFcJeU8Ys6wJyA',
    }
};
