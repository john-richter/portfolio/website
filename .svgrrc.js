module.exports = {
    prettier: false,
    prettierConfig: {
        parser: 'typescript'
    },
    svgo: true,
    expandProps: 'end',
    ref: true,
    // template({ template }, opts, { imports, componentName, props, jsx, exports }) {
    //     const typeScriptTpl = template.smart({ plugins: ['typescript'] });
    //     return typeScriptTpl.ast`
    //         ${imports}
    //         const ${componentName} = (props: React.SVGProps<SVGSVGElement>) => ${jsx};
    //         export default ${componentName};
    //     `;
    // }
}