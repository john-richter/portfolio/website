I had to move these out for reasons documented in src/config/muiStylesProviderProps.js

# Gatsby-Browser.js

```js
// See src/config/muiStylesProviderProps.js
import * as React from 'react';
import { ThemeProvider as MuiThemeProvider, StylesProvider } from '@material-ui/styles';
import STYLES_PROVIDER_PROPS from './src/config/muiStylesProviderProps';
// import { Providers } from './src/components/app/Providers';

export const wrapRootElement = ({ element }) => {
    return (
        <StylesProvider {...STYLES_PROVIDER_PROPS}>
            {element}
        </StylesProvider>
    );
}

module.exports = {
    wrapRootElement = ({element}) => {
        return <Providers
    }
};
```

# Gatsby-SSR.js

```js
// See src/config/muiStylesProviderProps.js

import * as React from 'react';
import { ThemeProvider as MuiThemeProvider, StylesProvider } from '@material-ui/styles';
import STYLES_PROVIDER_PROPS from './src/config/muiStylesProviderProps';
// import { Providers } from './src/components/app/Providers';

export const wrapRootElement = ({ element }) => {
    return (
        <StylesProvider {...STYLES_PROVIDER_PROPS}>
            {element}
        </StylesProvider>
    );
}

module.exports = {
    wrapRootElement = ({element}) => {
        return <Providers
    }
};
```