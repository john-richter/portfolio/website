# Welcome to my Digital Whiteboard

[![Netlify Status](https://api.netlify.com/api/v1/badges/2f3aa13d-076a-4ab3-a4f2-6f934946a34f/deploy-status)](https://app.netlify.com/sites/jrichter-io/deploys)

This site is currently deployed to [jrichter.io](https://jrichter.io) on Netlify.

Please check out the [release post](https://jrichter.io/blog/v2-acknowledgements/) and [project overview](https://jrichter.io/projects/personal-website-v2/) for more a bit of technical and background info.

Since I'm managing all versions of this website from the same repo, I'm loosely following [GitFlow](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow) to make it easier on myself.

I welcome new issues and PRs and will do my best to handle them all in a timely manner :)

I follow tech Twitter quite regularly. Let's [chat](https://twitter.com/jrichte43)!

**Tech**

- Typescript
- ReactJS
- GatsbyJS
- GraphQL

# Previous iterations of this website

**V1**

- Branch: [develop/v1](/tree/develop/v1)
- Website: [jrichter.io/v1](https://jrichter.io/v1)
