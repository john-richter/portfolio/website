// require('ts-node').register({ files: true }); // https://github.com/gatsbyjs/gatsby/issues/1457
require('dotenv').config({ path: `.env.${process.env.NODE_ENV}` });
const path = require('path');
const { getBlogFeed } = require('./src/config/gatsby/rssFeed');
const config = require('./src/config');
const { IMMUTABLE_CACHING_HEADER } = require('gatsby-plugin-netlify/constants');

module.exports = {
    siteMetadata: {
        // `site` prefixed names, title, and description are used within plugins for defaults.
        analytics: config.analytics,
        description: config.site.i18n.native.description,
        imageSeo: config.site.i18n.native.imageSeo,
        keywords: config.site.keywords,
        logo: config.site.logo,
        siteCanonicalUrl: config.site.canonicalUrl,
        siteName: config.site.i18n.native.name,
        siteUrl: config.site.url,
        social: config.social,
        title: config.site.i18n.native.title,
        titleSeo: config.site.i18n.native.titleSeo
    },
    plugins: [
        {
            resolve: 'gatsby-source-filesystem',
            options: {
                name: 'images',
                path: path.join(__dirname, 'src', 'assets'),
                ignore: ['**/*.md']
            }
        },
        {
            resolve: 'gatsby-source-filesystem',
            options: {
                name: 'data',
                path: path.join(__dirname, 'src', 'data'),
                ignore: ['**/*.md']
            }
        },
        {
            resolve: 'gatsby-source-filesystem',
            options: {
                name: 'content',
                path: path.join(__dirname, 'content')
            }
        },
        'gatsby-transformer-yaml',
        'gatsby-transformer-sharp',
        {
            resolve: 'gatsby-plugin-sharp',
            options: {
                useMozJpeg: false,
                stripMetadata: true,
                defaultQuality: 90
            }
        },
        'gatsby-plugin-svgr',
        {
            resolve: 'gatsby-plugin-material-ui',
            options: {
                pathToStylesProvider: 'src/config/muiStylesProviderProps'
                // If I want to use styled components I need to change the injection order here. See plugin docs.
            },
        },
        {
            resolve: 'gatsby-transformer-remark',
            options: {
                commonmark: true,   // CommonMark mode
                footnotes: true,    // Footnotes mode
                pedantic: true,     // Pedantic mode
                gfm: true,          // GitHub Flavored Markdown mode
                tableOfContents: {
                    maxDepth: 3     // Only show the first X headings in the TOC
                },
                plugins: [
                    {
                        // Used for linkes to files and images that gatsy-remark-image doesn't process
                        resolve: 'gatsby-remark-copy-linked-files',
                        options: {
                            // Filenames remain human readible with this config. Paths to files are hashes
                            destinationDir: f => `files/${f.hash}/${f.name}`,
                        }
                    },
                    {
                        resolve: `gatsby-remark-images`,
                        options: {
                            // It's important to specify the maxWidth (in pixels) of the content container as this
                            // plugin uses this as the base for generating different widths of each image.
                            maxWidth: 800,

                            // Wraps images in links to the file so they are clickable
                            linkImagesToOriginal: true,
                            showCaptions: ['title'],

                            // I want to set this to true, but there is a bug.
                            // https://github.com/gatsbyjs/gatsby/issues/16703
                            markdownCaptions: false,

                            // 'transparent' or a css value is also valid
                            backgroundColor: 'none',
                            quality: 90,
                            withWebp: true,
                            disableBgImageOnAlpha: true,

                            // replaces the "blur up" default effect. Check plugin docs before using
                            // tracedSVG: true,
                            // Valid values are 'lazy', 'eager', or 'auto'
                            loading: 'lazy',

                            // This is critical for my `gatsby-remark-rewrap-images` plugin and enables me to arrange
                            // image collection in flexbox rows that infinitely scale just like the GatsbyImgGrid
                            // component does manually for a single row of images.
                            wrapperStyle: (fluid) => `flex: ${fluid.aspectRatio} 1 0%;`
                        },
                    },
                    {
                        // Expects all images have been linked and transformed by gatsby-remark-copy-linked-files and
                        // gatsby-remark-images
                        resolve: 'gatsby-remark-rewrap-images',
                        options: {
                            // Keep in sync with `gatsby-remark-images::maxWidth` option
                            maxWidth: 800,
                        }
                    },
                    'gatsby-remark-responsive-iframe',
                    // Must appear before prismjs plugin if used.
                    // https://github.com/gatsbyjs/gatsby/issues/5764
                    'gatsby-remark-autolink-headers',
                    // Ensures that all external links are protected from the referrer security issues
                    'gatsby-remark-external-links',
                    {
                        // Replaces “dumb” punctuation marks with “smart” punctuation marks using retext-smartypants
                        resolve: 'gatsby-remark-smartypants',
                        options: {
                            dashes: 'oldschool'
                        }
                    },
                    // Provides themes for for `gatsby-remark-vscode`. Must appear before `gatsby-remark-vscode`
                    'gatsby-remark-vscode-customizations',
                    {
                        resolve: 'gatsby-remark-vscode',
                        options: {
                            theme: {
                                // default: 'Solarized Dark',
                                // default: 'Solarized Light',
                                default: 'Material Theme Palenight',
                                // default: 'Material Theme Lighter',
                                // dark: 'Material Theme Palenight',
                                // media: [{
                                //      match: '(prefers-color-scheme: light)',
                                //      theme: 'Material Theme Lighter High Contrast'
                                //  }]
                            },
                            injectStyles: false,
                            // Specify the extensions we want to use. Downloaded from the VSCode extension marketplace
                            extensions: [
                                // Enables the `gql` and `graphql` codes for highlighting GraphQL
                                `${__dirname}/plugins/gatsby-remark-vscode-customizations/extensions/kumar-harsh.graphql-for-vscode-1.15.3.vsix`
                            ]
                        }
                    },
                    // Wraps all of `gatsby-remark-vscode` `<pre>` blocks in a div. Must appear after
                    // `gatsby-remark-vscode`.
                    'gatsby-remark-vscode-monkeypatch',
                ],
            }
        },
        {
            resolve: 'gatsby-plugin-feed',
            options: {
                feeds: [
                    getBlogFeed({
                        siteUrl: config.site.canonicalUrl,
                        filePathRegex: '//content/blog//',
                        blogSlug: config.site.i18n.native.blog.slug,
                        output: config.site.i18n.native.blog.rssFeed,
                        // Only output references to the feed on `/blog/`
                        match: config.site.i18n.native.blog.injectRssFeedInPagesMatchingRegex,
                        title: config.site.i18n.native.blog.title,
                    }),
                ]
            }
        },
        {
            resolve: 'gatsby-plugin-manifest',
            options: {
                background_color: config.site.theme.backgroundColor,
                cache_busting_mode: config.site.cacheBustingMode,
                crossOrigin: config.site.crossOrigin,
                description: config.site.description,
                display: 'standalone',
                icon_options: config.site.icon.options,
                icon: config.site.icon.path,
                lang: config.site.language,
                name: config.site.title,
                short_name: config.site.titleShort,
                start_url: config.site.entrypoint,
                theme_color_in_head: config.site.theme.colorInHead,
                theme_color: config.site.theme.themeColor,
            }
        },
        // Gatsby allows both non-trailing and trailing slashes. This plugin removes all non-trailing slash page paths
        // and creates trailing slash versions if one doesn't already exist.
        // https://github.com/gatsbyjs/gatsby/issues/9207
        'gatsby-plugin-force-trailing-slashes',
        {
            resolve: 'gatsby-plugin-robots-txt',
            options: {
                resolveEnv: () => config.env,
                development: {
                    policy: [{ userAgent: '*', disallow: ['/'] }]
                },
                production: {
                    policy: [{ userAgent: '*', allow: '/' }]
                },
                'branch-deploy': {
                    policy: [{ userAgent: '*', disallow: ['/'] }],
                    sitemap: null,
                    host: null
                },
                'deploy-preview': {
                    policy: [{ userAgent: '*', disallow: ['/'] }],
                    sitemap: null,
                    host: null
                }
            }
        },
        {
            resolve: 'gatsby-plugin-sitemap',
            // Could create customizations to reduce the changefreq and priority for each page
            options: {}
        },
        {
            resolve: 'gatsby-plugin-google-tagmanager',
            options: {
                id: config.analytics.google.tagManager,

                // Include GTM in development. Defaults to false meaning GTM will only be loaded in production.
                includeInDevelopment: false,

                // `datalayer` to be set before GTM is loaded. Should be an object or a function that is executed in
                // the browser. Defaults to null
                // defaultDataLayer: { platform: 'gatsby' },

                // Specify optional GTM environment details.
                // gtmAuth: 'YOUR_GOOGLE_TAGMANAGER_ENVIRONMENT_AUTH_STRING',
                // gtmPreview: 'YOUR_GOOGLE_TAGMANAGER_ENVIRONMENT_PREVIEW_NAME',
                // dataLayerName: 'YOUR_DATA_LAYER_NAME',
            },
        },
        // Must be included after `gatsby-plugin-manifest`
        // (!) To remove this plugin, first deploy a version with `gatsby-plugin-remove-serviceworker` first
        //     See: https://www.gatsbyjs.org/packages/gatsby-plugin-offline/#remove
        'gatsby-plugin-offline',
        'gatsby-plugin-react-helmet',
        'gatsby-plugin-catch-links',
        'gatsby-plugin-typescript',
        {
            resolve: 'gatsby-plugin-netlify',
            options: {
                // Additional headers. `Link` headers are transformed by the below criteria
                headers: {
                    '/images/*': [IMMUTABLE_CACHING_HEADER],
                },

                // Additional headers for all pages. `Link` headers are transformed by the below criteria
                allPageHeaders: [],

                // Combine our security headers with the default security headers
                mergeSecurityHeaders: true,

                // Combine our headers with the default headers
                mergeLinkHeaders: true,

                // Combine our caching headers with the default caching headers
                mergeCachingHeaders: true,

                // Manipulate headers for all paths (e.g.sorting), etc.
                // transformHeaders: (headers, path) => headers,
                // Automatic creation of redirect rules for client only paths
                generateMatchPathRewrites: true,
            }
        },
        {
            resolve: 'gatsby-plugin-netlify-cache',
            options: {
                cachePublic: false,
                extraDirsToCache: [
                    'public/files',
                    'public/icons',
                    'public/images',
                    'public/static',
                ]
            }
        }
    ]
};
