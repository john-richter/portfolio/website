# Loading fonts correctly

We're loading our fonts via css (instead of css-in-js) because
 - Prevents Gatsby/Webpack from inlining as base64 blobs. Inlined blobs were corrupted in practice and I believe
   they aren't cacheable
    - Firefox would throw `downloadable font: rejected by sanitizer`
    - Chrome would throw, but render the font preview correctly in the network tab of the inspector
      - `Failed to decode downloaded font: <URL>`
      - `OTS parsing error: invalid version tag`
      - `Failed to decode downloaded font: data:font/woff2;base64,<encoded font>`
    - Safari would just hide the fact that something broke or it actually accepted the value.

If we had an easy way to force webpack to skip file inlining for fonts it would solve the issue. Perhaps there is
a strange file truncation issue with `url-loader` potential truncation of files really close to the 10kB limit. It
was possible to override the loader config for specific files, but this seemed error prone and hacky compared to
just loading regular css.

     import Exo300Woff2 from 'url-loader?{"limit": 1}!../../assets/fonts/Exo_300.woff2';

When troubleshooting this a SO post said that single quotes might be the issue. E.g `url('...')` vs `url("...")`
 - https://stackoverflow.com/questions/11072655/firefox-font-face-fail-with-fontawesome

We could load them in gatsby-browser.js, but for now lets just do it this way
 - It ensures they are loaded as early as possible and stay loaded across page navigation

# Dealing with LFS

Update: After a lot of digging and hair loss I've pinpointed the issues above to a combination of Git LFS, GitLab,
and Netlify.

## Git LFS is still pretty basic.

1. It doesn't have 'native' SSH support. All SSH requests are transformed to HTTP at runtime.

   https://github.com/git-lfs/git-lfs/issues/1044

2. It depends on the remote being named `origin`.

   If this doesn't exist LFS starts getting very flaky. I'd expect it to just stop working outright, but in my
   experience it worked fine for everything, but font files (woff2, woff, eot, and ttf).

   See the man pages for `git-lfs-fetch` and `git-lfs-checkout`

## GitLab is special?

1. GitLab handles font files in weird ways.

   Maybe they are trying to avoid being a font CDN? I'm not confident on this one, buy why else would every file
   work fine except for the font files? Images never gave me an issue. Perhaps GitLab serves them with the wrong
   mimetype?

        6:08:18 PM: $ git lfs fetch --all; git lfs checkout; cat /opt/build/repo/.git/lfs/logs/*
        6:08:18 PM: fetch: 100 object(s) found, done
        6:08:18 PM: fetch: Fetching all references...
        6:08:18 PM: batch request: missing protocol: ""
        6:08:18 PM: error: failed to fetch some objects from ''
        ...
        6:08:19 PM: Skipped checkout for "src/assets/fonts/Exo_300.eot", content not local. Use fetch to download.
        6:08:19 PM: Skipped checkout for "src/assets/fonts/Exo_300.ttf", content not local. Use fetch to download.
        6:08:19 PM: Skipped checkout for "src/assets/fonts/Exo_300.woff", content not local. Use fetch to download.
        6:08:19 PM: Skipped checkout for "src/assets/fonts/Exo_300.woff2", content not local. Use fetch to download.


## Netlify

1. They don't really support LFS outside of their own [`Large Media`](https://docs.netlify.com/large-media/overview/)
   product that well.

   An environment variable needs to be set **in the web UI** to enable proper LFS support (`GIT_LFS_ENABLED=true`).
   This seems to set up the `smudge` and `filter` git config and switches to `git lfs clone` (a deprecated tool) instead
   of `git clone`.

   Try to `git lfs clone` a repo and you'll get the deprecation warning.

   The problem is if you don't do this the Netlify build cache will cause every build to fail because of Git #2 above.
   You can manually tell Netlify to retry the build without the cache and the build will go normally.

   Further more when combined with GitLab #1 and Git #1 and #2 above you aren't guaranteed that LFS replaces all the
   file pointers with the real files. This was manifesting with Gatsby/Webpack base64 URL inlining my font URL with the
   content of the pointer itself. Browsers would scream (~24 warnings on each page load) saying the font was invalid and
   a potential security risk. It was because it decoded into the file pointer, not the font.

    - `Failed to decode downloaded font: <URL>`
    - `OTS parsing error: invalid version tag`
    - `Failed to decode downloaded font: data:font/woff2;base64,<encoded font>`

## Solution

To get the fonts to load properly, we have to:

1. Add the default remote, `origin`, to the checked out git repo using the **HTTP** protocol, not the SSH one.

        git remote add origin https://gitlab.com/johnrichter/personal-website.git

2. Force LFS to download all the latest refs and replace the links for real files.

        # Alternatively `git lfs fetch --all && git lfs checkout`
        git lfs pull

3. Finally, build the site using `gatsby build`

### What this means

I had thought using webpack and the `url-loader` directly by importing CSS files would
[fix the issues](#Loading_fonts_correctly) so I removed the  CSS-in-JS method that `src/lib/theme/fonts.ts` and
`src/components/app/Baseline.tsx` had employed. Now that I know the issue is really LFS, GitLab, and Netlify I can
state with confidence that the CSS-in-JS method should work just fine. I haven't tested it because, well, it works now
and I don't want to fuck with it. I could move the CSS loading to `gatsby-browser.js` as an optimization, but that's
for another day.

# Support @ Netlify

I made a [support request](https://community.netlify.com/t/constant-and-frustrating-build-issues-with-lfs/5350) to get help
