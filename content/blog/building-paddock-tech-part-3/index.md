---
title: Building Paddock Tech — Part 3
spoiler: Learning new languages while building a product is hard.
description: A breakdown of my progress since the beginning, road blocks, and struggles I've had while iterating on a product from the ground up.
shareMessage: From then to now. Dealing with road blocks and struggles of iterating on a brand new product.
publishDate: 2019-03-18
modifiedDate: 2019-03-18
author: johnrichter
hero: ./images/hero.jpg
hashtags:
  - entrepreneurship
  - hardwork
  - motorsport
tags:
  - Paddock Tech
  - Entrepreneurship
  - Engineering
---

## ["The App"](/projects/trackside/)

### What did it take to get where you are?

A lot of iteration, learning, and trial and error. I'm not a front end (UI/UX) designer or engineer so I've had to pick up and learn the tricks of the trade as I go. Learning how to develop for the web is a *deep*, **dark** rabbit hole of misinformation which makes it a serious challenge to get even the most basic stuff done.

I've been maniacally consuming the firehose of information about [JavaScript](), [Typescript](), and [React]() along with many other languages and frameworks it takes to build a maintainable, scalable, and production ready cross-platform web application. Trying to support every web browser used over the past five years on as many operating systems as possible all while keeping up with the rapid pace the web development ecosystem progresses is maddening.

An abridged timeline from what I have recorded in [GitLab](https://gitlab.com/).

1. Spring 2017 (after hours from my full time job)
    * Start working on the build system, based on [Webpack v3](https://webpack.js.org/)
    * Start learning about Javascript, Typescript, CSS, Web design
2. Summer 2017
    * Switch gears to a design-first methodology. What could the app look like? Is it feasible? Currently focused on it being a social lap time leaderboard.
    * Start drawing really crappy mockups and wireflow diagrams with Sketch to show people to gauge interest and get feedback.
    * After getting feedback reevaluate that a lap timer will be legally challenging and I need to pivot.
3. Fall 2017
    * Using inspiration from the app we used at [HOD's](https://hookedondriving.com/) 2017 [VIR South](https://virnow.com/) event I pivoted the app to being an at-the-track schedule tracker.
    * Purchased a lifetime membership to [ui8.net](http://ui8.net) and started using their design resources to create some nice mockups of what I wanted to build.
    * Had to learn what [SVG](https://en.wikipedia.org/wiki/Scalable_Vector_Graphics) is, how to manipulate it, and how to draw it. Picked up [Affinity Designer](https://affinity.serif.com/en-us/designer/) as my vector graphic program of choice.
4. Winter 2017-2018
    * Finished the mockups and started updating the original build system I had built back in spring 2017. So much had changed that I had to nearly do it all over again.
    * Started creating React/HTML skeletons to try to understand how the design could be implemented. Still had a rudimentary knowledge of React and web dev in general
    * Started creating basic implementations for things like buttons, checkboxes, forms, etc.
    * Started creating basic implementation of pages like account creation, track listings, etc. Everything looks like 1990s [Netscape](https://en.wikipedia.org/wiki/Netscape)
    * Try to keep my build system and app dependencies up-to-date. The rate of iteration and releases that these dependencies experience is hard to keep up with. Delete my build system entirely and switch to a more professional tool called [`Create React App`](https://github.com/facebook/create-react-app) from Facebook. It didn't support Typescript so I had to manually modify it and keep it in sync manually, but it is much more reliable.
    * Attempt to create `date` and `time` pickers as well as `selects` more times than I probably should have. These are excruciatingly hard to get right.
    * Add automated testing and QA into my build pipeline
    * Add content filtering features for various listings around the app: `tracks`, `events`, etc
    * Learn how to render and manipulate SVG images within the app
5. Spring 2018
    * Take March and most of April off to recover from burnout. Having a full time job while doing this on the side is exhausting.
    * Continue creating basic pages that are a blast to the Netscape past
    * More iteration on building a `select` box that I can use everywhere. Seems like a trend, no? A big problem is the lack of Typescript support in open source libraries.
    * Start having issues managing internal state with React's [built in method](https://reactjs.org/docs/react-component.html#setstate) in more complex pages like ones that let you create events.
    * Figured out how to render track maps with clickable turns and corners
    * Develop a [bin-packing algorithm](https://en.wikipedia.org/wiki/Bin_packing_problem) which enables me to create the [Gantt Chart](https://en.wikipedia.org/wiki/Gantt_chart)-like event timeline view
    * Create a super over-engineered (in hindsight) event timeline chart in pure SVG. This felt pretty damn cool when I finished it and I'm still proud of it.
6. Summer 2018
    * Build a page for admins to modify and create events on the fly. This is the final page that I wanted to build before saying that the logic is done enough to move on to styling and other tasks.
    * Full time job is crazy and burnout sets back in. The pretty epic 2018 track season begins. Development pauses.
7. Fall 2018
    * Show off my nice mockups, got feedback, and start talking with [Gran Touring Motorsports Chiefs](https://www.gtmotorsports.org/?page_id=19) and others about the potential for the app. The results were very nearly "shut up and take my money" which was a huge relief after working for so long and so hard on a prototype.
    * At this point I had been looking to get a new job as I was unhappy at the one I had.
    * After discussions with Erin, trusted coworkers, family, and friends I learned I had their full support to try to start a business from nothing and chase my dreams. This was invaluable to me.
    * Started taking the business aspect **much** more seriously. Followed along with [Startup School](https://www.startupschool.org/)'s first online cohort, watched, listened, and read everything I could about starting a business, strategizing, understanding markets, growth strategies, and more. This reinforced my previous experience going through the DoD's [I-Corps](https://www.nsf.gov/news/special_reports/i-corps/) program.
    * Started to document my vision, mission, company culture, story, pitch, financing strategy, doing market research, competitor research and analysis, and more. Started creating [Business Models Canvases](https://www.strategyzer.com/books/business-model-generation) and [Value Proposition Designs](https://www.strategyzer.com/books/value-proposition-design) for my target customer segments. Asked friends and family to reach out to their network to see if people would like to have interviews with me so I could validate my assumptions on my customer segments.
    * Made a budget and plan to build the business after I left my job and made November 15, 2018 my last day at work.
8. Winter 2018-2019
    * I've had more emotional ups and downs than I thought humanly possible, serious props to Erin for keeping me sane.
    * This year has been all about CSS and learning how to style websites using the new [CSS-in-JS](https://en.wikipedia.org/wiki/CSS-in-JS) paradigm.
    * I started off using a library called [Glamor](https://glamorous.rocks/) created by Paypal, but that was deprecated so I made the move to their suggested replacement, [Emotion](https://emotion.sh). It took a bit to figure out how to use these libraries with Typescript, but in the end it was a success and things were looking less Netscapey. It was still pretty poor since I was learning CSS the hard way. A kit from [ui8.net](http://ui8.net) was helping me handle the colors, spacing, and more.
    * I finally broke down in early February and started looking for existing libraries that I could use and customize to look the way I wanted to. I ended up discovering an amazing gem, the [`Material UI`](https://material-ui.com/) library. It was built by the open source community to be a highly customizable implementation of the Material UI [style guidelines](https://material.io/) (made by Google) for React with fabulous Typescript support. After I switched to this it took me ~3 weeks to replace every page I'd written before with their components, with my own custom styles, the ability to theme and more. I plan to donate time or money to the maintainers at some point in the future it is _that_ good.

Today, I'm finally starting to build the backend servers and moving to a database for all of my data. This means that an alpha version is visible on the horizon. I'm good at backend servers and APIs. 🤘
