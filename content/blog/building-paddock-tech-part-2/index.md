---
title: Building Paddock Tech — Part 2
spoiler: Setting goals is important
description: Motivations behind Paddock Tech and how am I staying on track to achieve the goals I've set for Paddock Tech?
shareMessage: Defining and pursuing goals for Paddock Tech
publishDate: 2019-03-15
modifiedDate: 2019-03-15
author: johnrichter
hero: ./images/hero.jpg
hashtags:
  - entrepreneurship
  - business
  - motorsport
tags:
  - Paddock Tech
  - Entrepreneurship
---

## What is the goal?

To build better track day experiences. I will consider this all a success if I can make the organizer's, instructor's, and driver's lives even a little bit easier. I'd love to increase communication across the board and create a system which naturally increases the quality of on-track instruction.

I'd love to increase interest and participation in track days and amateur racing in the younger generations. If I can build a platform that reduces the cost of running an event as well as increases the visibility of all types of motorsport events I believe that my company has the potential to achieve this goal. The more newcomers I can find and attract to events the longer our sport flourishes.

The amount of people I've heard that know someone interested in track days or are interested themselves, but don't know where to start is staggering. Let's make it **easy**. Let's remove as much **friction** and make it as inviting as we can.

## What processes are you using to be successful?

I'm focusing on running as **lean** as possible. I need to do this in the most efficient way possible as I'm living off of my savings until I can start bringing in enough money to pay myself.

* Project Management &mdash; I'm simply using [GitLab](https://gitlab.com/) to track progress in an Agile manner. It is pretty Waterfall right now until v1 of the app is released, but you can find `Milestones`, `Stories`, `Features`, and `Issues` spread across the various repositories that make up the product.
* Business Plan &mdash; Most of my ideas are semi-documented in my [Notion](https://notion.so/) account along with a bunch of other topics like Vision, Pitch, Market Research, Marketing, Culture, etc
* Business Model &mdash; I'm using the [Business Model Canvas](https://www.strategyzer.com/books/business-model-generation) and [Value Proposition Canvas](https://www.strategyzer.com/books/value-proposition-design) by [Strategyzer](https://www.strategyzer.com/). I highly recommend them if you are interested in this kind of stuff. It is very similar to the [Steve Blank](https://steveblank.com/) / [Lean Startup](http://theleanstartup.com/) / [I-Corps](https://www.nsf.gov/news/special_reports/i-corps/) training I've had in the past and I find it to be quite efficient. My office closet is littered with sticky notes.

![Too many sticky notes](./images/StickyNotes.jpeg)
