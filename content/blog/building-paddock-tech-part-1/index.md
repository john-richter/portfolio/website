---
title: Building Paddock Tech — Part 1
spoiler: Building a company is fun they said
description: Where did the idea come from and why start a company is a notoriously difficult niche market?
shareMessage: Starting a new type of motorsports company in a notoriously difficult niche market
publishDate: 2019-03-13
modifiedDate: 2019-03-13
author: johnrichter
hero: ./images/hero.jpg
hashtags:
  - entrepreneurship
  - motorsport
  - fullsend
tags:
  - Paddock Tech
  - Entrepreneurship
---

## Forward

Welcome to the inaugural post of my new blog! I want to bring everyone up to speed and into the loop on what I've been building over the past two years so we can make Motorsports more fun and more accessible together. Over the next few days I'll be diving a bit deeper into [Paddock Tech](https://paddock.tech/), the current state of the app I'm building (with pictures!), the reasoning behind my technical decisions and what those were, timelines, and more.

Let's begin with a bit of history.

## Where did this all start?

About halfway through my first job after college I've wanted to try my hand at building a product and a business, but I had never had an idea worth pursuing until now. The timeline I'll share in the next few days as a part of this series will give you a better view into the steps and missteps I've taken so far.

One of my favorite things about [Gran Touring Motorsports](https://gtmotorsports.org/) is that we unofficially compete against each other by tracking our lap times on a leaderboard. Initially (Spring 2017), I took that concept and started ruminating on what it would take to replace our member-sourced Google Sheets database of lap times with some simple software that equalized everything by the major variables (e.g. power, weight, environmental conditions, and tires) automatically. Since we all can't always go to the same events I saw it as an opportunity to increase participation across our membership by reducing some friction. I mean, someone has to officially beat [Eric](https://www.gtmotorsports.org/?page_id=2951&um_user=16) amirite?! One can dream...

In fall of 2017, Eric found out that [Audi Club of North America](https://audiclubna.org/) had their own simple app which helped drivers manage their schedule at an event and wanted to try it out. After a bit of outreach, they agreed to support [Hooked on Driving's](https://www.hookedondriving.com/) [VIR: South](https://virnow.com/track/configurations/) event later that season. Long story short, it was a chaotic event that made it a perfect candidate to test how functional and well an app could simplify and assist drivers, instructors, and staff. It was at this event that I showed a couple of people my designs for a lap timer app and while the idea went over alright, several large issues such as the definition of `competition` including `timing` would lead to complications with [HPDE](https://en.wikipedia.org/wiki/High_Performance_Driver_Education) organization adoption due to insurance and liability. As a result, I made the decision to table the lap timer for the time being and explore the schedule management app idea which has become the main focus of my efforts today.

## Why start a company?

First, I want to vastly improve track days for everyone. Imagine if

* Your track experience was automatically recorded, updated, and usable by the Chief Instructors across all organizations
* Students could be automatically introduced to their instructors and have discussions before and after an event
* All participants could rate the event host, track, instructors, and staff
* There was one single place to discover and register for track events run by all of your favorite organizations around the world
* At the track you had a live, interactive schedule, and could check in automatically without signing waivers and waiting in lines
* You had all of the above **at your fingertips**

From a personal standpoint this is something that would make my track day much more engaging and I strongly believe that club racers to bucket listers would greatly benefit from a platform like this.

Second, I believe that I've stumbled onto an untapped market, however small it may be, and I know I have the capability and expertise to fill the gap. The east coast has some of the best access to race tracks in the country which makes it relatively easy to attend events and make my feedback and iteration loop as quick and tight as possible. This is critical to building a great, successful product.

Finally, my personality demands it. I'd rather have my bosses be my customers rather than some manager with an obscure task list that needs to be completed or a vision that I can't get behind. I believe that is the most important thing I've learned in the first 10 years of my career.
