---
title: Website 2.0 and Acknowlegements
spoiler: Leveling up my digital whiteboard thanks to a wonderful community.
description: I've finally designed and engineered my own custom website powered by Gatsby and React❣️
shareMessage: Great examples of well designed personal websites and resources to design your own pixel perfect portfolio
publishDate: 2019-11-17
modifiedDate: 2019-11-22
author: johnrichter
hero: ./images/hero.jpg
hashtags:
  - portfolio
  - design
  - inspiration
tags:
  - Engineering
---

## Release Day!

It has taken a while to get this done, but damn it feels good. I put a pretty good amount of effort into this one so I could really grow my design skills and intuition. It was a great excuse to finally dive deep into [Gatsby](https://gatsbyjs.com) to see how I could use it for [Paddock Tech](https://paddock.tech) in the future. Verdict &mdash; it's amazing. Prerendering is really the way to go with React.

I've written more about this revision over on its [project page](/projects/personal-website-v2/) for those interested. The short and sweet is

* It's entirely written in [Typescript](https://www.typescriptlang.org/)
* I'm using the [Material UI](https://material-ui.com) component library for React
* Any state management I need to do outside of [Gatsby](https://gatsbyjs.com) and [GraphQL](https://graphql.org/) is [Mobx](https://mobx.js.org/) and `React.useState()`
* GraphQL types are generated using [GraphQL Code Generator](https://graphql-code-generator.com/)
* The content for the blog is written in Markdown
* I built my style guide using [Figma](https://www.figma.com/) and a bunch of guidance from [Refactoring UI](https://refactoringui.com/)
* Fonts featured are [Exo](https://fonts.google.com/specimen/Exo) and [Inter](https://rsms.me/inter/). Depending on your OS you'll see Menlo, Monaco, [Consolas](https://docs.microsoft.com/en-us/typography/font-list/consolas), or [Roboto Mono](https://fonts.google.com/specimen/Roboto+Mono) for monospaced elements. Icon sets include [Font Awesome](https://fontawesome.com/), and [Material Icons](https://material.io/resources/icons).
* I discovered the most adorable way to combine my initials (JR) &#128054;

## Acknowledgements

While I'm really proud of what I've accomplished, it wouldn't have been possible without a bunch of incredibly talented people who have put the time and energy into their own personal websites and open sourced them. I've [done the same](https://gitlab.com/johnrichter/personal-website) hoping that my efforts can help someone like me in the future.

A **special** thanks goes to [Steve Schoger](https://twitter.com/steveschoger) and [Adam Wathan](https://twitter.com/adamwathan) who's [Refactoring UI](https://refactoringui.com/) book provided invaluable advice for a developer like me to level up my design and UI game. It was **worth every dollar** it and will help me keep getting better for years to come (perhaps very soon on [Trackside](/projects/trackside/)).

Without any further adieu, thank you

![Brittany Chiang](./images/brittanychiang.jpeg)
![Aaron Noel De Leon (Nelo)](./images/nelo-light.jpeg)

![Max Böck](./images/mxb-light.jpeg)
![Julian Shapiro](./images/julianshapiro.jpeg)
![Dan Abramov](./images/overreacted-light.jpeg)

![Kent C. Dodds](./images/kentcdodds.jpeg)
![Paul Stamatiou](./images/paulstamatiou.jpeg)

![Justin Jackson](./images/justinjackson.jpeg)
![Andrew Askins](./images/andrewaskins.jpeg)
![Stuart Balcombe](./images/stuartbalcombe.jpeg)

[Brittany Chiang](https://brittanychiang.com/), [Aaron Noel De Leon (Nelo)](https://nelo.is/), [Max Böck](https://mxb.dev/), [Julian Shapiro](https://www.julian.com/), [Dan Abramov](https://overreacted.io), [Kent C. Dodds](https://kentcdodds.com), [Paul Stamatiou](https://paulstamatiou.com/), [Justin Jackson](https://justinjackson.ca/), [Andrew Askins](https://www.andrewaskins.com/), and [Stuart Balcombe](https://www.stuartbalcombe.com/), you all are **incredible**. You've all inspired or helped me technically in some way during the development of this website. Keep up the great work &mdash; you've gained long time fan!
