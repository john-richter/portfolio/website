---
title: Building Paddock Tech — Part 4
spoiler: Iterating quickly requires an open mind
description: Take a look at the current iteration of the Paddock Tech app and the milestones I've set for future development.
shareMessage: Get a sneak peak into how far Paddock Tech has come and where it is going
publishDate: 2019-03-20
modifiedDate: 2019-03-20
author: johnrichter
hero: ./images/hero.jpg
hashtags:
  - entrepreneurship
  - motorsport
  - hardwork
tags:
  - Paddock Tech
  - Entrepreneurship
---

## What are you working on now?

Here is where we stand at the end of Milestone #1 (description is after the pictures). I've captured some gifs to give everyone a taste of the functionality the app has at this point. I personally think it will help me at a track day and I hope the same will be true for everyone else. There are some main design considerations that have directed me throughout development.

1. Most people will be using the app on their phone, during the day.
    - It is crucial for the UX work well and as expected for a mobile app.
    - You must be able to see it in the sun. To that end I've added a dark and a light theme that are easily toggled throughout.
2. People will want to use the app at home, on their larger devices to find future events that are interesting to them in addition to using the listings to get to the event schedule on the day of.

Please leave a comment if you like, dislike, or have ideas!

![Listing racetracks](./images/TrackListing-Mobile1.gif)
![Listing organizations and event hosts](./images/OrgListing-Mobile1.gif)

![Searching for events](./images/EventListing-Mobile1.gif)
![Searching for events](./images/EventListing-NonMobile1.gif)

![Viewing an event at the track](./images/EventDetail-Mobile1.gif)
![Viewing an event from a computer](./images/EventDetail-NonMobile1.gif)

![Viewing an event from a computer](./images/EventDetail-NonMobile2.gif)
![Viewing an event from a computer](./images/EventDetail-NonMobile3.gif)

## What's up next?

The plan this week (and last) is to start being more transparent with everyone. I've realized that my heads-down development and tight lips are only going to negatively effect the product and the community I wish to build. I want to build this app **with people** not **for people**.

One of the biggest obstacles I've had in my career thus far has been the unnecessary secrecy of every facet of product development and company decisions, The lack of communication that kind of behavior causes is something I want to avoid plague. I'm going to be making regular updates and I hope everyone holds me to that.

I am a bit worried about an overzealous cadence might be too much for me to really write anything of substance, but maybe that is a good thing as I can just write small "I did a thing and here's a picture!" updates which are more consumable.

As of March 10th, I've finished Milestone [`#1`](#milestones) below and will be starting with Milestone [`#2`](#milestones). Up until now I've had to manually rebuild the website for each page I wanted to look at. This was fine when I was spending all of my time building that particular page, but now that effort is done, I need to actually wire everything up so that it functions like a real app and website.

The lack of routing in the app has been driving me nuts so this will be a welcome first step into making the app more dynamic and natural. After that I need to update the data structure that represents a `Track` so that it has a default `Circuit`. That way I don't have to deal with the hassle of safely indexing directly into arrays and can just reference the "main" circuit when rendering the list of tracks, for example.

### Milestones

Here are the next few milestones I'm currently working towards. Time estimation is never perfect 😭 , but I'm working as fast as I can!

1. (Completed — Late) Prototype: Styled Static Website (Mid Feb)

    The goal was to have a website using all hard coded data, fully functional, and looking pretty. The final color scheme and branding to come before launch. I demoed the near final version of this at a recent GTM party.

    > I missed this one by about 3 weeks due to the switch to the Material UI library. It was worth it 👌🏻

2. (In progress — Late) Prototype: Dynamic Website (Mid March)

    The goal is to have all hard coded data removed from the UI and being retrieved from a database, a database built and populated with data, and an API server the UI accesses to access the database.

    > I'm well on my way with this milestone, but since I didn't start until March 12th it's undoubtedly already late. At least backend servers and APIs are my forte 😁

3. Alpha (Beginning of April) — In danger of being late

    The goal is to have a functioning website that I can have people start playing with to get some hard feedback.

4. Beta (Mid April) — On schedule

    The goal is to take the feedback and anything else I want to change, update the app, and push it out to get feedback again.

5. Launch (End of April) — On schedule

    The goal is to take the feedback from the beta, create a finalized version 1, and release it in time for the main track season to start.

## Q&A

### When can we expect to start using and testing the app?

Sometime in April 😉

### How will I download the app?

When it is ready I'll post a URL to the website that you go to to start using it. Since it is an offline-enabled web app, just open up your browser to access the app.

Check out [part 5](/blog/building-paddock-tech-part-5/) of this series which details my design decisions on why I didn't create Android or iOS apps.

### How can I help test the app?

The best way is to use it like you normally use any other app and let me know if things break or aren't a great experience. I'd also love to know how the app is improving your track day as well as any ideas you might have to make it better for everyone. For pre-releases, I'll be distributing a username and password that you can use to access a special website.

### How do I submit feedback?

For now, [tweet at me](https://twitter.com/jrichte43) or start a discussion using the links at the bottom of this page. When the app is launched there will be a way in the app to submit feedback.

### Can I get a copy of the track maps you are using in the app?

Sure! I've uploaded them as an [icon set on Icon Finder](https://www.iconfinder.com/iconsets/motorsport-racing-circuits) that you can buy to support me. If you want to customize one or have a special one made for you please get in [touch with me](mailto:connect@jrichter.io). I'd be glad to help!